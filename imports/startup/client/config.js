//import { AccountsTemplates } from "meteor/useraccounts:core";
//import { Router } from "meteor/iron:router";

AccountsTemplates.configure({
	defaultLayout: 'layout'
});

AccountsTemplates.configureRoute('signIn', {
	name: 'signIn',
	path: '/login',
	layoutTemplate: 'layout',
	redirect: '/'
});

AccountsTemplates.configureRoute('signUp', {
	name: 'signUp',
	path: '/register',
	layoutTemplate: 'layout',
	redirect: '/'
});

AccountsTemplates.addField({
	_id: 'firstname',
	type: 'text',
	displayName: 'First Name',
	required: true,
	minLength: 3,
	errStr: 'error.minChar',
	trim: true
});

AccountsTemplates.addField({
	_id: 'lastname',
	type: 'text',
	displayName: 'Last Name',
	required: true,
	minLength: 3,
	errStr: 'error.minChar',
	trim: true
});